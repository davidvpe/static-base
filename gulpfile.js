const { src, dest, task, watch, series, parallel } = require('gulp')
const merge = require('merge-stream')
const del = require('del')
const options = require('./config')
const browserSync = require('browser-sync').create()
const path = require('path')
const fs = require('fs/promises')
const buffer = require('vinyl-buffer')

const sass = require('gulp-sass')(require('sass'))
const bourbon = require('node-bourbon').includePaths
const postcss = require('gulp-postcss')
const concat = require('gulp-concat')
const uglify = require('gulp-uglify')
const imagemin = require('gulp-imagemin')
const cleanCSS = require('gulp-clean-css')
const purgecss = require('gulp-purgecss')
const sourcemaps = require('gulp-sourcemaps')
const autoprefixer = require('gulp-autoprefixer')
const panini = require('panini')

const browserify = require('browserify')
const babelify = require('babelify')
const source = require('vinyl-source-stream')
const glob = require('glob')
const Polypipe = require('polypipe').default

const gulpif = require('gulp-if')
const htmlmin = require('gulp-htmlmin')

const gulpEjsMonster = require('gulp-ejs-monster')

const nodepath = 'node_modules/'

//Note : Webp still not supported in major browsers including forefox
//const webp = require('gulp-webp'); //For converting images to WebP format
//const replace = require('gulp-replace'); //For Replacing img formats to webp in html
const logSymbols = require('log-symbols') //For Symbolic Console logs :) :P

const purgeCSSOptions = {
    content: ['src/**/*.{ejs,js}'],
    defaultExtractor: (content) => {
        const broadMatches = content.match(/[^<>"'`\s]*[^<>"'`\s:]/g) || []
        const innerMatches =
            content.match(/[^<>"'`\s.()]*[^<>"'`\s.():]/g) || []
        return broadMatches.concat(innerMatches)
    },
}

//Load Previews on Browser on dev
function livePreview(done) {
    browserSync.init({
        server: {
            baseDir: options.paths.dist.base,
        },
        port: options.config.port || 5000,
    })
    done()
}

function sassCompile(glob, outputFile, outputFolder, prod) {
    return src(glob)
        .pipe(
            sass({
                outputStyle: 'compressed',
                sourceComments: 'map',
                sourceMap: 'scss',
                includePaths: [
                    bourbon,
                    `${options.paths.src.base}/scss/utilities/`,
                ],
            }).on('error', sass.logError)
        )
        .pipe(autoprefixer('last 2 version', 'safari 5', 'ie 8', 'ie 9'))
        .pipe(gulpif(prod, purgecss(purgeCSSOptions)))
        .pipe(gulpif(!prod, sourcemaps.init()))
        .pipe(concat(outputFile + '.css'))
        .pipe(gulpif(prod, cleanCSS({ compatibility: 'ie8' })))
        .pipe(gulpif(!prod, sourcemaps.write('./')))
        .pipe(dest(outputFolder))
        .pipe(gulpif(!prod, browserSync.stream()))
}

async function scanAndCompileCSS(outputFolder, prod) {
    const scssFolder = path.resolve(options.paths.src.base, 'scss')

    let processes = await fs.readdir(scssFolder).then((filePaths) => {
        const avoided = ['utilities']
        const promises = filePaths
            .filter((filePath) => !avoided.includes(filePath))
            .map((filePath) => {
                return fs
                    .stat(path.resolve(scssFolder, filePath))
                    .then((stats) => {
                        if (stats.isDirectory()) {
                            console.log('process folder', filePath)
                            return sassCompile(
                                [`src/scss/${filePath}/*.scss`],
                                filePath,
                                outputFolder,
                                prod
                            )
                        } else {
                            console.log('process', filePath)
                            return sassCompile(
                                [`src/scss/${filePath}`],
                                path.parse(filePath).name,
                                outputFolder,
                                prod
                            )
                        }
                    })
            })

        return Promise.all(promises)
    })

    return merge(processes)
}

function devSCSS() {
    console.log('\n\t' + logSymbols.info, 'Compiling App SCSS..\n')
    return scanAndCompileCSS(options.paths.dist.css, false)
}

//Compile HTML partials with Panini
function devHTML() {
    console.log('\n\t' + logSymbols.info, 'Compiling HTML..\n')
    panini.refresh()
    return src('src/views/pages/**/*.ejs')
        .pipe(
            gulpEjsMonster({
                layouts: 'src/views',
                widgets: 'src/views',
                requires: 'src/views',
                includes: 'src/views',
            })
        )
        .pipe(dest('dist'))
        .pipe(browserSync.stream())
}

//Concat CSS Plugins

const vendorCSS = [nodepath + 'owl.carousel/dist/assets/*.min.css']

function compileVendorCSS(outputFolder, prod) {
    return src(vendorCSS)
        .pipe(gulpif(!prod, sourcemaps.init()))
        .pipe(concat('vendor.css'))
        .pipe(gulpif(prod, purgecss(purgeCSSOptions)))
        .pipe(gulpif(!prod, sourcemaps.write('./')))
        .pipe(dest(outputFolder))
        .pipe(gulpif(!prod, browserSync.stream()))
}
function devCSSPlugins() {
    console.log('\n\t' + logSymbols.info, 'Compiling Plugin styles..\n')
    return compileVendorCSS(options.paths.dist.css, false)
}

//Reset Panini Cache
function resetPages(done) {
    console.log('\n\t' + logSymbols.info, 'Clearing Panini Cache..\n')
    panini.refresh()
    done()
}

//Triggers Browser reload
function previewReload(done) {
    console.log('\n\t' + logSymbols.info, 'Reloading Browser Preview.\n')
    browserSync.reload()
    done()
}

//Optimize images
function devImages() {
    return src(`${options.paths.src.img}/**/*`).pipe(
        dest(options.paths.dist.img)
    )
}

function browserifyHelper(globPath) {
    var files = glob.sync(globPath)
    return browserify({
        entries: files,
        noParse: [`jquery`],
        //require: { jquery: 'jquery-browserify' },
        // Pass babelify as a transform and set its preset to @babel/preset-env
        transform: [
            babelify.configure({
                presets: ['@babel/preset-env'],
            }),
        ],
    })
}
function processJS(glob, outputFile, outputFolder, minify) {
    return (
        browserifyHelper(glob)
            // Bundle it all up!
            .bundle()
            // Source the bundle
            .pipe(source(outputFile))
            .pipe(buffer())
            .pipe(gulpif(minify, uglify()))
            // Then write the resulting files to a folder
            .pipe(dest(outputFolder))
    )
}
async function findAndCompileJS(outputFolder, minify) {
    let processes = await fs.readdir(options.paths.src.js).then((filePaths) => {
        const promises = filePaths.map((filePath) => {
            return fs
                .stat(path.resolve(options.paths.src.js, filePath))
                .then((stats) => {
                    if (stats.isDirectory()) {
                        return processJS(
                            `${options.paths.src.js}/${filePath}/**/*.js`,
                            filePath + '.bundle.js',
                            outputFolder,
                            minify
                        )
                    } else {
                        return processJS(
                            `${options.paths.src.js}/${filePath}`,
                            filePath,
                            outputFolder,
                            minify
                        )
                    }
                })
        })

        return Promise.all(promises)
    })

    return merge(processes)
}

// Let's write our task in a function to keep things clean
async function devJavascript() {
    // Start by calling browserify with our entry pointing to our main javascript file
    //var bundleThis = function (srcArray) {
    return findAndCompileJS(options.paths.dist.js, false)
}

function devFonts() {
    console.log('\n\t' + logSymbols.info, 'Copying fonts to dist folder.\n')
    return src([`${options.paths.src.fonts}/*`])
        .pipe(dest(options.paths.dist.fonts))
        .pipe(browserSync.stream())
}

function watchFiles() {
    watch(
        `${options.paths.src.base}/views/**/*`,
        series(devHTML, previewReload)
    )
    watch(['src/scss/**/*', 'src/scss/*'], devSCSS)
    watch(
        `${options.paths.src.js}/**/*.js`,
        series(devJavascript, previewReload)
    )
    watch(`${options.paths.src.img}/**/*`, series(devImages, previewReload))
    console.log('\n\t' + logSymbols.info, 'Watching for Changes..\n')
}

function devClean() {
    console.log(
        '\n\t' + logSymbols.info,
        'Cleaning dist folder for fresh start.\n'
    )
    return del([options.paths.dist.base])
}

function prodFonts() {
    console.log('\n\t' + logSymbols.info, 'Copying fonts to dist folder.\n')
    return src([`${options.paths.src.fonts}/*`])
        .pipe(dest(options.paths.build.fonts))
        .pipe(browserSync.stream())
}

function prodSCSS() {
    console.log('\n\t' + logSymbols.info, 'Compiling App SCSS..\n')
    return scanAndCompileCSS(options.paths.build.css, true)
}

function prodCSSPlugins() {
    console.log('\n\t' + logSymbols.info, 'Compiling Plugin styles..\n')
    return compileVendorCSS(options.paths.build.css, true)
}

//Production Tasks (Optimized Build for Live/Production Sites)
function prodHTML() {
    console.log('\n\t' + logSymbols.info, 'Compiling HTML..\n')
    panini.refresh()
    return src('src/views/*.ejs')
        .pipe(
            gulpEjsMonster({
                layouts: 'src/views',
                widgets: 'src/views',
                requires: 'src/views',
                includes: 'src/views',
            })
        )
        .pipe(
            htmlmin({
                collapseWhitespace: true,
                minifyCSS: true,
                minifyJS: true,
                minifyURLs: true,
            })
        )
        .pipe(dest(options.paths.build.base))
}

function prodJavascript() {
    return findAndCompileJS(options.paths.build.js, true)
}

function prodImages() {
    return src(options.paths.src.img + '/**/*')
        .pipe(imagemin())
        .pipe(dest(options.paths.build.img))
}

function prodClean() {
    console.log(
        '\n\t' + logSymbols.info,
        'Cleaning build folder for fresh start.\n'
    )
    return del([options.paths.build.base])
}

function buildFinish(done) {
    console.log(
        '\n\t' + logSymbols.info,
        `Production build is complete. Files are located at ${options.paths.build.base}\n`
    )
    done()
}

exports.default = series(
    devClean, // Clean Dist Folder
    resetPages,
    parallel(
        devFonts,
        devCSSPlugins,
        devSCSS,
        devJavascript,
        devImages,
        devHTML
    ),
    livePreview, // Live Preview Build
    watchFiles // Watch for Live Changes
)
exports.prod = series(
    prodClean, // Clean Dist Folder
    resetPages,
    parallel(
        prodFonts,
        prodCSSPlugins,
        prodSCSS,
        prodJavascript,
        prodImages,
        prodHTML
    ),
    buildFinish
)
